import sys

fordered = ("raw", "bmp", "tiff", "ppm", "eps", "tga", "pdf", "svg", "png", "gif", "jpeg", "webp")

def lower_than(format1, format2):
    return fordered.index(format1) < fordered.index(format2)

def find_lower_pos(formats, pivot):
    min_index = pivot
    for i in range(pivot + 1, len(formats)):
        if lower_than(formats[i], formats[min_index]):
            min_index = i
    return min_index

def sort_formats(formats):
    for i in range(len(formats)):
        pivot = find_lower_pos(formats, i)
        formats[i], formats[pivot] = formats[pivot], formats[i]
    return formats

def main():
    formats: list = sys.argv[1:]
    for format in formats:
        if format not in fordered:
            sys.exit(f"Formato inválido: {format}")
    sorted_formats: list = sort_formats(formats)
    for format in sorted_formats:
        print(format, end= " ")
    print()

if __name__ == "__main__":
    main()
